from csvprocess import *
import unittest

class TestCSVProcess(unittest.TestCase):
    def testLectureFichier(self):
        fileTestContent = [["ref.","name","price","release-date"], ["IEL-51619","Ishtar - Les Jardins De Babylone","40.0","2019-10-04"], ["BLU-KIB01","Kingdomino","19.90","2016-10-21"], ["B0-DRA01","Draftosaurus","19.90","2019-02-22"], ["FLY004JU","Jurassic Snack","19.90","2018-04-06"]]
        self.assertEqual(lectureFichier("tests/test1.csv"), fileTestContent)
    
    def testEcritureFichier(self):
        self.assertTrue(ecritureFichier("tests/test1.csv", lectureFichier("tests/test1.csv")))

    def testModifColonnes(self):
        fileTestContent = [["adresse_titulaire", "nom", "prenom", "immatriculation", "date_immatriculation", "vin", "marque", "denomination_commerciale", "couleur", "carrosserie", "categorie", "cylindree", "energie", "places", "poids", "puissance", "type", "variante", "version"], ["3822 Omar Square Suite 257 Port Emily, OK 43251", "Smith", "Jerome", "OVC-568", "03/05/2012", "9780082351764", "Williams Inc", "Enhanced well-modulated moderator", "LightGoldenRodYellow", "45-1743376", "34-7904216", "3462", "37578077", "32", "3827", "110", "Inc", "92-3625175", "79266482"], ]
        self.assertEqual(modifColonnes(lectureFichier("tests/test2.csv")), fileTestContent)